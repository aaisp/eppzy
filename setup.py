from setuptools import setup

with open('README.md') as f:
    long_description = f.read()

setup(
    name='eppzy',
    description='EPP Client Library',
    long_description=long_description,
    long_description_content_type='text/markdown',
    keywords=['epp'],
    url='https://gitlab.com/aaisp/eppzy',
    author='David Honour',
    author_email='david.honour@aa.net.uk',
    version='0.0.15',
    license='LGPL3',
    packages=['eppzy'],
    install_requires=[
    ],
    extras_require={
        'test': ['pytest', 'pytest-flake8', 'mock']
    },
    entry_points={}
)
