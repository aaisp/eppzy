import logging

from eppzy.session import session
from eppzy.rfc5733_contact import Contact
from eppzy.nominet import NContact

logging.basicConfig(level=logging.DEBUG)

ses = session(
    [Contact], [NContact], 'ote-epp.nominet.org.uk', 700, 'userid', 'password')
with ses as s:
    print(s['contact'].create(
        'contactid', 'Connie', 'Company', 'Street', 'City', 'County',
        'SB12 5NU', 'GB', '+44.11899988199', 'faxno', 'connie@example.com'))
