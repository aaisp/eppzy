from eppzy.nominate import nominate_session

with nominate_session('api.nominate.com', 700, 'user', 'pass') as s:
    print(s['domain'].check(['example.com']))
    print(s['domain'].info('nominate.com'))
