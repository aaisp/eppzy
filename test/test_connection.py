from contextlib import contextmanager
from io import BytesIO
import socket
import ssl
from threading import Thread

from mock import patch
import pytest

from eppzy.connection import (
    connect, ProxyConnectionError, ReadUntilSocket, tag, length
)


class FakeSocket:
    def __init__(self, content, recv_size):
        self._recv_buf = BytesIO(content)
        self._recv_size = recv_size

    def recv(self, n):
        if self._recv_buf.tell() == len(self._recv_buf.getvalue()):
            raise AssertionError('At end of buffer')
        return self._recv_buf.read(min(n, self._recv_size))


buf_sizes = (
    (1024, 1024),
    (1024, 4),
    (4, 1024),
    (4, 4),
    (3, 4),
    (4, 3),
)


@pytest.mark.parametrize('socket_recv_size, rus_recv_size', buf_sizes)
def test_read_until_tag(socket_recv_size, rus_recv_size):
    s = FakeSocket(
        b'the quick\r\nbrown fox\r\njumps over\r\n', socket_recv_size)
    rus = ReadUntilSocket(s)
    rus.recv_size = rus_recv_size
    assert rus.read_until(tag(b'\r\n')) == b'the quick'
    assert rus.read_until(tag(b'\r\n')) == b'brown fox'
    assert rus.read_until(tag(b'\r\n')) == b'jumps over'


@pytest.mark.parametrize('socket_recv_size, rus_recv_size', buf_sizes)
def test_read_until_length(socket_recv_size, rus_recv_size):
    s = FakeSocket(
        b'the quick\r\nbrown fox\r\njumps over\r\n', socket_recv_size)
    rus = ReadUntilSocket(s)
    rus.recv_size = rus_recv_size
    assert rus.read_until(length(10)) == b'the quick\r'
    assert rus.read_until(length(6)) == b'\nbrown'
    assert rus.read_until(length(18)) == b' fox\r\njumps over\r\n'


def expect(s, bs):
    assert s.recv(len(bs)) == bs


@pytest.mark.parametrize(
    'proto,ip', (
        (socket.AF_INET, '0.0.0.0'),
        (socket.AF_INET6, '::'),
    )
)
@patch.object(ssl, 'wrap_socket', lambda x: x)
@patch('eppzy.connection.ReadUntilSocket.recv_size', 4)
def test_connect(proto, ip):
    def serve(s):
        s2c, _ = s.accept()
        expect(s2c, b'hi')
    with serve_ctx(proto, ip, serve) as (bound_ip, bound_port):
        c2s = connect(bound_ip, bound_port, proto)
        c2s.send(b'hi')


@pytest.mark.parametrize(
    'proto,ip', (
        (socket.AF_INET, '0.0.0.0'),
        (socket.AF_INET6, '::'),
    )
)
@patch.object(ssl, 'wrap_socket', lambda x: x)
@patch('eppzy.connection.ReadUntilSocket.recv_size', 4)
def test_proxy_connect(proto, ip):
    def serve(s):
        s2c, _ = s.accept()
        expect(s2c, b"CONNECT foo.com:1234 HTTP/1.1\r\n\r\n")
        s2c.send(b"HTTP/1.1 200 OK\r\n\r\n")
        expect(s2c, b"hi")
    with serve_ctx(proto, ip, serve) as (bound_ip, bound_port):
        c2s = connect('foo.com', 1234, proto, bound_ip, bound_port)
        c2s.send(b"hi")


@pytest.mark.parametrize(
    'proto,ip', (
        (socket.AF_INET, '0.0.0.0'),
        (socket.AF_INET6, '::'),
    )
)
@pytest.mark.parametrize(
    'bad_response, err_msg', (
        (b"BOOOOOOOO", 'Malformed HTTP response'),
        (b"Bad HTTP header", 'Malformed HTTP response'),
        (b"PTTH/0.1 400 Not HTTP tag", 'Malformed HTTP response'),
        (b"HTTP/1.0 400 Go Away", 'HTTP CONNECT failed'),
    )
)
@patch('eppzy.connection.ReadUntilSocket.recv_size', 4)
def test_proxy_connect_failure(proto, ip, bad_response, err_msg):
    def serve(s):
        s2c, _ = s.accept()
        expect(s2c, b"CONNECT foo.com:1234 HTTP/1.1\r\n\r\n")
        s2c.send(bad_response + b'\r\n\r\n')
    with serve_ctx(proto, ip, serve) as (bound_ip, bound_port):
        with pytest.raises(ProxyConnectionError) as exc_info:
            connect('foo.com', 1234, proto, bound_ip, bound_port)
        assert exc_info.value.args[0] == err_msg


@contextmanager
def serve_ctx(proto, ip, serve_f):
    with socket.socket(proto, socket.SOCK_STREAM) as s:
        s.bind((ip, 0))
        bound_ip, bound_port, *_ = s.getsockname()
        s.listen()

        t = Thread(target=lambda: serve_f(s))
        t.start()
        try:
            yield bound_ip, bound_port
        finally:
            t.join()
