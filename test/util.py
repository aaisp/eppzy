from os.path import dirname, join


def data_file_contents(relpath):
    p = join(dirname(__file__), relpath)
    with open(p) as f:
        return f.read().encode('utf8')
